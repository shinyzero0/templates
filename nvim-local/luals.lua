opts = { -- {{{

	settings = {

		Lua = {

			runtime = {
				version = "LuaJIT",
			},
			diagnostics = {

				disable = {
					"lowercase-global",
				},
				globals = { "vim" },
			},
			workspace = {

				library = {

					vim.api.nvim_get_runtime_file("", true),
					vim.fn.stdpath("config"),
				},
				checkThirdParty = false, -- THIS IS THE IMPORTANT LINE TO ADD
			},
			telemetry = {
				enable = false,
			},
		},
	},
} -- }}}
LSP_SETUP("lua_ls", opts)
