opts = { -- {{{

	cmd = {

		"OmniSharp",
		"--languageserver",
		"--hostPID",
		tostring(vim.fn.getpid()),
	},

	on_attach = function(client, bufnr)
		client.server_capabilities.semanticTokensProvider = nil
	end,
} -- }}}

LSP_SETUP("omnisharp", opts)
