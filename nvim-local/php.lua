local opts = {
	cmd = { "./node_modules/intelephense/lib/intelephense.js", "--stdio" },
}
require("lspconfig")["intelephense"].setup(vim.tbl_deep_extend("keep", opts, LSP_STDOPTS))
